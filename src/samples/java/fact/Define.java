package fact;

/*
 * Пример демонстрирует инициализацую класс, как аткора.
 * При запуске актора, через метод Model.launch(instance), 
 * актор создается в системе, если у класса объявлен метод с именем define, 
 * то данный метод будет вызван первым.
 */
public class Define {

	public void define() {
		System.out.println("hello actor!");
	}
	
	public static void main(String...args) {
		Model.launch(new Define());
	}
}
