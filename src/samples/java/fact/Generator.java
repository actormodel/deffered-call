package fact;

public class Generator implements Context {
	private int value = 0;
	
	public void define() {
		send("generator", create("even", new EvenOrOdd()));
	}
	
	public void generator(Reference target) {
		target.send("isEven", value++);
		send("generator", target);
	}
	
	public static void main(String...args) {
		Model.launch(new Generator());
	}
	
	class EvenOrOdd {
		private Reference out;
		
		public void define() {
			out = create("out", System.out);
		}
		
		public void isEven(int value) {
			if(value % 2 == 0)
				out.send("println", value + " is even");
			else
				out.send("println", value + " is odd");
		}
	}
}
