package fact;

import java.util.Random;

public class FaultSample implements Context {

	public void define() {
		Reference ref = create("test", new Array());
		for(int i = 0 ; i < 11 ; i++)
			ref.send("test", i)
			.handle("handle")
			.fault("error");
		send("after");
	}
	
	public void after() {
		System.out.println("after");
	}
	public void handle(int v) {
		System.out.println(v);
	}
	
	public void error(Throwable e) {
		System.out.println(e);
	}
	
	public class Array {
		private int[] test = new int[10];
		
		{ 
			Random random = new Random();			
			for(int i = 0 ; i < 10 ; i++)
				test[i] = random.nextInt();
		}
		
		public int test(int i) {
			return i;
		}
	}
	
	
	public static void main(String...args) {
		Model.launch(new FaultSample());
	}
}
