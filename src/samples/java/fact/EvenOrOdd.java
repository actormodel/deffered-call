package fact;


public class EvenOrOdd implements Context {
	private int value = 0;
	
	public void define() {
		send("tick");
	}
	
	public void tick() {
		send("isEven", value++);
		send("tick");
	}
	
	public void isEven(int value) {
		if(value % 2 == 0)
			System.out.println(value + " is even");
		else
			System.out.println(value + " is odd");
	}
	
	public static void main(String...args) {
		Model.launch(new EvenOrOdd());
	}
}
