package fact;

/*
 * Можно создать в системе, любой объект, например консольный вывод.
 * Через
 */
public class Console implements Context {

	public void define() {
		final Reference console = create("console", System.out);
		for(char c : "hello world!".toCharArray())
			console.send("println", c);
	}
	
	public static void main(String...args) {
		Model.launch(new Console());
	}
}
