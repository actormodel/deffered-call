package fact;

public class CallbackSample implements Context {

	public void define() {
		create("test", new Test())
		.send("test")
		 .handle("handler");
		send("tick");
	}
	
	public void tick() {
		System.out.println("after");
	}
	
	public void handler(int value) {
		System.out.println("receive: " + value);
	}
	
	class Test {
		private int value;
		
		public int test() {
			return value++;
		}
	}
	
	public static void main(String...args) {
		Model.launch(new CallbackSample());
	}
}
