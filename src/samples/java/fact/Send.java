package fact;

public class Send implements Context {

	public void define() {
		for(int i = 0 ; i < 100 ; i++)
			if(i % 2 == 0)
				send("even", i);
			else 
				send("odd", i);
	}
	
	public void even(int a) {
		System.out.println("test: " +a);
	}
	
	public void odd(int x) {
		System.out.println("x: " + x);
	}
	
	public static void main(String...args) {
		Model.launch(new Send());
	}
}
