package fact.samples.dispatcher;

import fact.Context;
import fact.Model;
import fact.TFactory;

public class SingleDispatcherSample implements Context, TFactory {
	public int value = 0;
	
	public void define() {
		for(int i = 0 ; i < 100 ; i++)
			create("test-"+i, new Test(i));
	}
	
	public class Test {
		private final int index;
		
		public Test(int index) {
			this.index = index;
		}
		
		public void define() {
			send("tick");
		}
		
		public void tick() {
			value++;
			System.out.println("#" + index + " : " + value);
			value--;
			send("tick");
		}
	}
	
	public static void main(String...args) {
		Model.launch(new SingleDispatcherSample());
	}
}
