package fact;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;

public class Dispatcher {
	private LinkedList<Call> list = new LinkedList<>();
	
	private Call target;
	
	public synchronized Call dispatch(Call call) {
		if(target == null) {
			process(target = call);
		} else
			list.add(call);
		return target;
	}
	
	public synchronized Call release(Call call) {
		if(target == call) {
			if(!list.isEmpty())
				process(target = list.removeFirst());
			else
				target = null;
		} else
			System.out.println("dispatcher release problem");
		return target;
	}
	
	private final void process(Call call) {
		ForkJoinPool.commonPool().execute(call);
	}	
	
	static interface Factory {
		Dispatcher create();
	}
	
	static class SharedDispatcherFactory implements Factory {
		private final Dispatcher dispatcher = new Dispatcher();
		
		@Override
		public Dispatcher create() {
			return dispatcher;
		}
	}
	
	static class SingleDispatcherFactory implements Factory {
		@Override
		public Dispatcher create() {
			return new Dispatcher();
		}
	}

}
