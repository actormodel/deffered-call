package fact;

import java.util.Arrays;

class Call implements Runnable {
	final Record source;
	
	final Record target;
	
	final String name;
	
	final Object[] content;
	
	Class<?>[] signature = null;
	
	final Callback callback;
	
	final boolean response;
	
	Call(Record source, Record target, String name, Object[] content, Callback callback, boolean response) {
		this.source = source;
		this.target = target;
		this.name = name;
		this.content = content;
		this.callback = callback;
		this.response = response;
	}
	
	
	Call(Record source, Record target, String name, Object[] content, Callback callback) {
		this(source, target, name, content, callback, false);
	}

	@Override
	public void run() {
		Worker.current().setContext(this);
		try {
			Object result = target.process(this);
			if(callback != null && !response)
				callback.callback(result, false);
		} catch(Throwable e) { 
			if(callback != null && !response)
				callback.callback(e, true); 
			else
				e.printStackTrace();
		}
		target.free();
	}

	//return call arguments signature
	//if is first call generate signature array, and return it
	public Class<?>[] signature() {
		if(signature == null) {
			if(content == null)
				signature = new Class<?>[0];
			else {
				signature = new Class[content.length];
				for(int i = 0 ; i < content.length ; i++)
					signature[i] = content[i].getClass();
			}
		}
		return signature;
	}
	
	//validation method pattern signature call
	boolean validate(Class<?>[] pattern) {
		if(signature().length != pattern.length)
			return false;
		for(int i = 0 ; i < pattern.length ; i++)
			if(!check(pattern[i], signature[i]))
				return false;
		return true;
	}
	
	static boolean check(Class<?> left, Class<?> right) {
		if(left.isPrimitive()) {
			if(right == Boolean.class && left == boolean.class) return true;
			else if(right == Byte.class && left ==  byte.class) return true;
			else if(right == Short.class  && left ==  short.class) return true;
			else if(right == Integer.class  && left ==  int.class) return true;
			else if(right == Long.class && left ==  long.class ) return true;
			else if(right == Float.class && left ==  float.class ) return true;
			else if(right == Double.class && left ==  double.class) return true;
			else if(right == Character.class && left ==  char.class) return true;
			else return false;
		} else
			return left.isAssignableFrom(right);
	}
	
	@Override
	public String toString() {
		return source + " " + target + " " + name + " " + Arrays.toString(content);
	}
	
	
}