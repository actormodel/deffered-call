package fact;

public interface TFactory {

	/**
	 * При создании актора, в системе, генерирует заданный тип обработчика, для установленного класса. 
	 * @param type тип обработчика "shared", "single"
	 * @param clazz класс для которого устанавливается тип обработчика
	 */
	default void dispatcher(String type, Class<?> clazz) {
		Worker.current().target().setDispatcherFactory(type, clazz);
	}
	
}
