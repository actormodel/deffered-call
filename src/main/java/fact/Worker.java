package fact;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

public class Worker extends ForkJoinWorkerThread{
	private Call context;

	private final LinkedList<Callback> buffer = new LinkedList<>();
	
	protected Worker(ForkJoinPool pool) {
		super(pool);
		setDaemon(false);
	}

	public static Worker current() {
		return (Worker) Thread.currentThread();
	}

	void setContext(Call call) {
		if(context != null)
			new RuntimeException();
		context = call;
	}

	void clear() {
		buffer.forEach(item -> item.call());
		buffer.clear();
		context = null;
	}
	

	Callback out(String name, Object[] content) {
		return out(context.target, name, content);
	}
	
	Callback out(Record target, String name, Object[] content) {
		final Callback c = new Callback(context.target, target, name, content);
		buffer.add(c);
		return c;
	}

	Reference create(String name, Object instance) {
		return context.target.create(name, instance);
	}

	Record target() {
		return context.target;
	}

}
