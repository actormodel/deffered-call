package fact;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.invoke.MethodHandles.Lookup;
import java.util.HashMap;
import java.util.LinkedList;

import org.pmw.tinylog.Logger;

public class Record {
	private Lookup lookup = MethodHandles.lookup();
	
	private final LinkedList<Call> mailbox = new LinkedList<Call>();
	
	private final Record parent;
	
	final String name;//actor name
	
	private final Object instance;//instance of actor
	
	private final Reference reference = new Reference(this);//reference on current actor record
	
	private final String address;//full record address
	
	private final Dispatcher dispatcher;
	
	Record(Record parent, String name, Object instance, Dispatcher dispatcher) {
		this.parent = parent;
		this.name = name;
		this.instance = instance;
		address = ((parent != null) ? parent.address + "/" : "") + name;
		this.dispatcher = dispatcher;
	}
	
	synchronized void in(Call call) {
		Logger.trace("#in: {}", this);
		if(call.target != this)
			throw new RuntimeException(call.target + " " + this);
		if(target == null && callbacks.isEmpty()) {
			dispatcher.dispatch(target = call);
		} else
			mailbox.add(call);
	}
	
	private Call target = null;
	
	public Object process(Call call) throws Throwable {
		Logger.trace("#process: {} call: {}", this, call);
		if(target != call)
			Logger.debug("process not waiting message");
		for(Method m : instance.getClass().getDeclaredMethods()) {
			if(m.getName().equals(call.name) && Modifier.isPublic(m.getModifiers()) && !Modifier.isStatic(m.getModifiers())) {
				if(call.validate(m.getParameterTypes())) {
					return lookup.bind(instance, call.name, MethodType.methodType(m.getReturnType(), m.getParameterTypes())).invokeWithArguments(call.content);
				}
			}
		}
		throw new RuntimeException("unhandle behavior " + call);
	}
	
	private final LinkedList<Callback> callbacks = new LinkedList<>();
	
	synchronized void free() {
		Worker.current().clear();
		Logger.trace("#free: {}", this);
		if(target != null) {
			if(target.response) {
				if(!callbacks.remove(target.callback))
					System.out.println("callbacks remove problem");
				dispatcher.release(target);
				if(callbacks.isEmpty()) {
					if(!mailbox.isEmpty())
						dispatcher.dispatch(target = mailbox.removeFirst());
					else
						target = null;
				} else
					if(!mailbox.isEmpty()) {
						if(mailbox.peekFirst().response)
							dispatcher.dispatch(target = mailbox.removeFirst());
						else
							target = null;
					} else
						target = null;
			} else {
				dispatcher.release(target);
				if(!mailbox.isEmpty() && callbacks.isEmpty())
					dispatcher.dispatch(target = mailbox.removeFirst());
				else
					target = null;
			}
		} else
			throw new RuntimeException("Попытка освободить не занятый актор");
	}
	
	private final HashMap<String, Record> childs = new HashMap<>();
	
	
	private final Dispatcher newDispatcher(Class<?> clazz) {
		Dispatcher.Factory factory = dispatcherFactory.get(clazz);
		if(factory != null)
			return factory.create();
		else
			return new Dispatcher();
	}
	
	Reference create(String name, Object instance) {
		final Record record = new Record(this, name, instance, newDispatcher(instance.getClass()));
		synchronized (childs) {
			if(!childs.containsKey(name)) {
				childs.put(name, record);
				record.define();
			} else
				System.out.println("actor with address `" + address + "/" + name + "` alredy exists");
		}
		return record.reference;
	}

	private void define() {
		if(target == null) {
			boolean define = false;
			//search define method in instance class
			for(Method m : instance.getClass().getDeclaredMethods())
				if(m.getName().equals("define") && m.getReturnType() == void.class && m.getParameterTypes().length == 0) {
					define = true;
					break;
				}
			if(define)
				in(new Call(parent, this, "define", null, null));
		} else
			System.out.println("problem with actor defining");
	}

	synchronized void callback(Callback callback, Object result, boolean failure) {
		if(!callbacks.isEmpty()) {
			if(callbacks.contains(callback)) {
				final String name = failure ? callback.fault : callback.handler;
				if(name == null)
					Logger.warn("dont setuped callback handler result: " + result + " failure: " + failure);
				if(target == null)
					dispatcher.dispatch(target = new Call(callback.target, this, name, new Object[]{result}, callback, true));
				else 
					mailbox.addFirst(new Call(callback.target, this, name, new Object[]{result}, callback, true));
			} else
				System.out.println("receive wrong callback " + callback);
		} else
			System.out.println("callback problems");
	}
	
	
	@Override
	public String toString() {
		return instance.getClass() +  ": " + address;
	}

	void out(Callback callback) {
		Logger.trace("#out: from {}", this);
		if(callback.fault != null || callback.handler != null)
			callbacks.add(callback);
	}
	
	//хранит все установленные правила для фабрики
	private final HashMap<Class<?>, Dispatcher.Factory> dispatcherFactory = new HashMap<>();
	
	void setDispatcherFactory(String type, Class<?> clazz) {
		Dispatcher.Factory factory;
		switch(type) {
		case "shared": factory = new Dispatcher.SharedDispatcherFactory(); break;
		case "single": factory = new Dispatcher.SingleDispatcherFactory(); break;
		default: factory = new Dispatcher.SingleDispatcherFactory(); break;
		}//TODO log if wrong type
		dispatcherFactory.put(clazz, factory);
		
	}
}
