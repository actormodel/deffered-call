package fact;

public class Callback {
	final Record source;
	
	final Record target;
	
	private final String name;
	
	private final Object[] content;
	
	String handler = null;
	
	String fault = null;
	
	private Call call;
	
	Callback(Record source, Record target, String name, Object[] content) {
		this.source = source;
		this.target = target;
		this.name = name;
		this.content = content;
	}
	
	public Callback handle(String methodName) {
		if(methodName == null)
			throw new RuntimeException();
		handler = methodName;
		return this;
	}
	
	public Callback fault(String methodName) {
		if(methodName == null)
			throw new RuntimeException();
		fault = methodName;
		return this;
	}
	
	void call() {
		source.out(this);
		target.in(call = new Call(source, target, name, content, this));
	}
	
	void callback(Object result, boolean failure) {
		if(handler != null || fault != null)
			source.callback(this, result, failure);
	}
}
