package fact;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;
import java.util.concurrent.ForkJoinWorkerThread;

import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;

public class Model implements ForkJoinWorkerThreadFactory {
	private final Record record = new Record(null, "actor-system", this, new Dispatcher());
	
	@Override
	public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
		return new Worker(pool);
	}
	 
	/*
	 * Creating root instance in system.
	 * Note: For starting computation instance must contain public define method.
	 * @param instance root instance
	 */
	public static void launch(final Object instance) {
		Logger.getConfiguration().level(Level.DEBUG).activate();
		System.setProperty("java.util.concurrent.ForkJoinPool.common.threadFactory", Model.class.getName());
		((Model) ForkJoinPool.commonPool().getFactory()).start(instance);			
	}
	
	private void start(Object instance) {
		ForkJoinPool.commonPool().execute(() -> record.create("root", instance));
	}
}
