package fact;

public interface Context {

	default Reference create(String name, Object instance) {
		return Worker.current().create(name, instance);
	}
	
	default void send(String name, Object...content) {
		Worker.current().out(name, content);
	}
	
}
