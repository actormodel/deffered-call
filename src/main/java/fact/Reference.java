package fact;

public class Reference {
	private final Record record;
	
	Reference(Record record) {
		this.record = record;
	}
	
	public Callback send(String name, Object...content) {
		return Worker.current().out(record, name, content);
	}
	
}
